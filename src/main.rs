use std::fs;
use chrono::prelude::*;
use postmill::*;
use regex::Regex;
use rand::Rng;

fn main() {
    // Try to see if there is a post earlier than today
    let last_time = match fs::read_to_string("last_time.txt") {
        Ok(last) => DateTime::parse_from_rfc3339(&last.lines().next().unwrap()).unwrap(),
        Err(_) => {
            fs::write("last_time.txt", Utc::now().to_rfc3339()).unwrap();
            println!("No previous time found, writing new time!");
            return;
        }
    };

    let mut client = Client::new("https://raddle.me").unwrap();
    let mut login = false;

    let re = Regex::new(r"\\roll\s+(\d*)d(\d+)").unwrap();
    let mut rng = rand::thread_rng();

    // Get all the newest comments
    client.comments_until(last_time).unwrap()
        // Turn the vector into an iterator
        .iter()
        // Order the comments by last one first
        .rev()
        // Only keep the comments matching the regex
        .filter(|comment| re.is_match(&comment.body.to_lowercase()))
        // Finally post a comment
        .for_each(|comment| {
            // Login
            if !login {
                client.login("DiceBot", include_str!("../password.txt").trim()).unwrap();
                login = true;
            }

            let mut roll = String::new();

            for cap in re.captures_iter(&comment.body.to_lowercase()) {
                let times = cap[1].parse::<u32>().unwrap_or(1);
                if times > 100 {
                    client.reply_comment(comment.submission_id, comment.id, "A maximum of 100 throws is allowed.").unwrap();

                    fs::write("last_time.txt", comment.date.to_rfc3339()).unwrap();

                    return;
                }

                roll.push_str("\n---\n");

                let sides = cap[2].parse::<u32>().unwrap();
                for _ in 0 .. times {
                    let num: u32 = rng.gen_range(1, sides + 1);
                    roll.push_str(&*format!("{}  \n", num));
                }
            }

            // Send a reply comment
            let msg = format!("You rolled:\n{}", roll);
            client.reply_comment(comment.submission_id, comment.id, &*msg).unwrap();

            fs::write("last_time.txt", comment.date.to_rfc3339()).unwrap();
        });
}
